.sprite {
    background-image: url(spritesheet.png);
    background-repeat: no-repeat;
    display: block;
}

.sprite-ic_caledar {
    width: 19px;
    height: 19px;
    background-position: -14px -14px;
}

.sprite-ic_caledar-hover {
    width: 19px;
    height: 19px;
    background-position: -61px -14px;
}

.sprite-ic_forum {
    width: 28px;
    height: 20px;
    background-position: -108px -14px;
}

.sprite-ic_forum-_1_-hover {
    width: 28px;
    height: 20px;
    background-position: -164px -14px;
}

.sprite-ic_journey {
    width: 20px;
    height: 20px;
    background-position: -220px -14px;
}

.sprite-ic_journey-_1_-hover {
    width: 20px;
    height: 20px;
    background-position: -268px -14px;
}
